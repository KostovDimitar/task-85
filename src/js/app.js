import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready


  const ul = document.querySelector("ul");
  
  let url = 'https://pokeapi.co/api/v2/pokemon/?limit=10';
  let name;
  let array = [];

  //Resolve if HTTP Status code is 2** or reject if its not 2**
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response);
    }
    else {
      return Promise.reject(
        new Error(response.statusText)
      );
    }
  }

  //Convert the response to JSON
  function toJSON(response){
    return response.json();
  }

  //Check the HTTP Status code, convert to JSON, save the response to array,
  //Get the names assign them to a variable and create LI elements with the names
  fetch(url)
    .then(checkStatus)
    .then(toJSON)
    .then((data) => {
      array = [...array, ...data.results];
      for (const key in array){
        Object.entries(array[key]).forEach(([key, value]) => {
          if(key == 'name') name = [value];
        })
        const li = document.createElement('li');
        ul.appendChild(li);
        li.innerText = name;
      }
    })
});
